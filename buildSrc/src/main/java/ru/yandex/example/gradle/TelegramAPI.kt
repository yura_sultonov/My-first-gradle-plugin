package ru.yandex.example.gradle

import retrofit.client.Response
import retrofit.http.GET
import retrofit.http.Query
import java.io.File

interface TelegramAPI {
    @GET("/sendMessage?parse_mode=Markdown")
    fun sendMessage(
            @Query("text") msg: String,
            @Query("chat_id") charId: String,
            @Query("disable_notification") silent: Boolean
    ) : Response

    @GET("")
    fun sendFile(token: String, file: File) : Response
}